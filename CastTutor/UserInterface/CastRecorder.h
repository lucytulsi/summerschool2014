#pragma once
#include "DataSnap.h"
#include "Vec3.h"
#include <vector>

using namespace std;

class CastRecorder
{
public:
	CastRecorder();
	vector<dataSnap> _castRecording;
private:
	Vec3 _currentVelocity;
	dataSnap RecordDataSnap();
};

