#pragma once

class SocketInterface
{
public: 
	virtual void Create() = 0;
	virtual void Receive() = 0;
};
