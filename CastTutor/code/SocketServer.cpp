#include "SocketServer.h"

void SocketServer::Create()
{
  /* socket creation */
  _socketDescripter=socket(AF_INET, SOCK_DGRAM, 0);
  if(_socketDescripter<0) 
  {
    printf("cannot open socket \n");
    exit(1);
  }

  /* bind local server port */
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servAddr.sin_port = htons(LOCAL_SERVER_PORT);
  _bindResult = bind (_socketDescripter, (struct sockaddr *) &servAddr,sizeof(servAddr));
  if(_bindResult<0) 
  {
    printf("cannot bind port number %d \n", 
	   LOCAL_SERVER_PORT);
    exit(1);
  }
}

void SocketServer::Receive()
{
  printf("waiting for data on port UDP %u\n", 
	   LOCAL_SERVER_PORT);

    
    /* init buffer */
    memset(_data,0x0,MAX_MSG);

		/* receive message */
		cliLen = sizeof(cliAddr);
		n = recvfrom(_socketDescripter, _data, strlen(_data)+1, 0, 
			 (struct sockaddr *) &cliAddr, &cliLen);
			 
		if(n<0) 
		{
		  printf("cannot receive data \n");
		}
		  
		/* print received message */
		printf("from %s:UDP%u : %.10s \n", 
		   inet_ntoa(cliAddr.sin_addr),
		   ntohs(cliAddr.sin_port),_data);

}
