#pragma once

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h> /* close() */
#include <string.h> /* memset() */
#include <stdlib.h> // exit()

#include "SocketInterface.h"

#define LOCAL_SERVER_PORT 1500
#define MAX_MSG 100

class SocketServer : public SocketInterface
{
public:      
	void Create();
	void Receive();
	
private:
	int _socketDescripter, _bindResult, n;
	unsigned int cliLen;
    struct sockaddr_in cliAddr, servAddr;
    char _data[MAX_MSG];
};

  
