#pragma once

#include "Vec3.h"

class dataSnap
{
public:
	dataSnap();
 	Vec3 _velocity;
 	float _speed;
 
 	Vec3 getVelocity(Vec3 acceleration, Vec3 currentVelocity);
	float getSpeed();
};
