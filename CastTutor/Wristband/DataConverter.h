#pragma once
#include "SocketInterface.h"
#include "quad.h"

class DataConverter
{
public:
	DataConverter(SocketInterface *socket);
	
	void Send(char *data);
	void ConvertHextoInt(char accelerometerReading[6]);
	
	quad _convertedData;
	SocketInterface *_socket;
};
