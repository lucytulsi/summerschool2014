#include "DataConverter.h"
#include "SocketInterface.h"

DataConverter::DataConverter(SocketInterface *socket)
{
	_socket = socket;
}

void DataConverter::Send(char *data)
{
	_socket->Send(data);
}

void DataConverter::ConvertHextoInt(char accelerometerReading[6])
{
	short x = (accelerometerReading[1] << 8) + accelerometerReading[0] >>6;
	_convertedData.x = x >> 6;
	_convertedData.y = ((short)(accelerometerReading[1] << 8) + accelerometerReading[0]) >>6;
	_convertedData.z = ((short)(accelerometerReading[1] << 8) + accelerometerReading[0]) >>6;
}
