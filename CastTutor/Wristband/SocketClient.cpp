#include "SocketClient.h"

void SocketClient::Open(char *hostName, int port) 
{

	/* get server IP address (no check if input is IP address or DNS name */
	h = gethostbyname(hostName);
	if(h==NULL)
	{
		printf("unknown host '%s' \n", hostName);
		exit(1);
	}
  
	/* socket creation */
	socketDescripter = socket(AF_INET,SOCK_DGRAM,0);
	if(socketDescripter<0) {
		printf("cannot open socket \n");
		exit(1);
	}
  
	/* bind any port */
	cliAddr.sin_family = AF_INET;
	cliAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	cliAddr.sin_port = htons(0);
  
	bindResult = bind(socketDescripter, (struct sockaddr *) &cliAddr, sizeof(cliAddr));
	if(bindResult<0) {
		printf("cannot bind port\n");
		exit(1);
	}
  
	remoteServAddr.sin_family = h->h_addrtype;
	memcpy((char *) &remoteServAddr.sin_addr.s_addr, 
	h->h_addr_list[0], h->h_length);
	remoteServAddr.sin_port = htons(port);
}

void SocketClient::Send(char *data)
{
	printf("sending data to '%s' (IP : %s) \n", h->h_name,
		inet_ntoa(*(struct in_addr *)h->h_addr_list[0]));
		   
	  /* send data */
	bindResult = sendto(socketDescripter, data, strlen(data)+1, 0,    
		(struct sockaddr *) &remoteServAddr, sizeof(remoteServAddr));

	if(bindResult<0) 
	{
		printf("cannot send data\n");
		close(socketDescripter);
		exit(1);
	}
}
