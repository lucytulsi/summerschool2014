#pragma once

class SocketInterface
{
public:
	virtual void Open(char *, int) = 0;
	virtual void Send(char *) = 0;
};
