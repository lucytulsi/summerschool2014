#pragma once
#include "SocketInterface.h"

class MockSocketClient : public SocketInterface
{
public:
	void Open(char *hostName, int port)
	{
		_mockHostName = hostName;
		_mockPort = port;
	}
	void Send(char *data){_mockData = data;};
	
	char *_mockHostName;
	char *_mockData;
	int _mockPort;
};
