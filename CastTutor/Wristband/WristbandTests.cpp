#include <limits.h>
#include <gtest/gtest.h>
#include "MockSocketClient.h"
#include "DataConverter.h"
#include "quad.h"


MockSocketClient mockSocket;
MockSocketClient *mockSock = &mockSocket;
DataConverter dataConverter(mockSock);

TEST(DataConverter, dataConverter_sends_correct_data_to_socket)
{
	dataConverter._socket->Open("host name", 9);
	dataConverter.Send("message");
	
	ASSERT_EQ("host name", mockSock->_mockHostName);
	ASSERT_EQ(9, mockSock->_mockPort);
	ASSERT_EQ("message", mockSock->_mockData);
}

/* TEST(DataConverter, dataConverter_converts_hex_to_expected_int)
{
	    //  identifier, x, y, z,
	quad expected = {1, 1, 2, 3};
	
	char testHex[6];
	testHex[0] = 0x00;//x
	testHex[1] = 0x01;//x
	testHex[2] = 0x00;//y
	testHex[3] = 0x02;//y
	testHex[4] = 0x00;//z
	testHex[5] = 0x03;//z
	
	dataConverter.ConvertHextoInt(testHex);
	
	ASSERT_EQ(expected.x, dataConverter._convertedData.x);
	ASSERT_EQ(expected.y, dataConverter._convertedData.y);
	ASSERT_EQ(expected.z, dataConverter._convertedData.z);
}
*/
