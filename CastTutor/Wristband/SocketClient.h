#pragma once
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h> /* memset() */
#include <sys/time.h> /* select() */ 
#include <stdlib.h> // exit()
#include "quad.h"
#include "SocketInterface.h"
#define REMOTE_SERVER_PORT 1500
#define MAX_MSG 100

class SocketClient : public SocketInterface
{
public:      
	void Open(char *hostName, int port);
	void Send(char *data);
	
private:
	int socketDescripter, bindResult;
    struct sockaddr_in cliAddr, remoteServAddr;
    struct hostent *h;
};
